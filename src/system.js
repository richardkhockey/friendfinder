import './styles.scss';
import PageSetup from './js/PageSetup';
import FriendFinder from './js/FriendFinder.js';

PageSetup();

window.consoleLog = true;
window.p2LiveMode = false;

// google maps loaded and ready for use
function initphase1(){
	// get your location

	let pos = false;

	let friendfinder = new FriendFinder({
		"what3wordsAPIkey" : "2GNBA9OG",
		"googleAPIKey" : "AIzaSyDuFF1X2RZ7rzFejeTUp-ZlFjenju_DkJA",
		"mapcontainer" : ".googlemap"
	});

	if (p2LiveMode) {
		// live location lookup
		if (navigator.geolocation) {
			// try{
				navigator.geolocation.getCurrentPosition(function(position) {
					pos = {
						"lat" : position.coords.latitude,
						"lng" : position.coords.longitude
					};

					friendfinder.phase1(pos);

				}, function() {
					// geoloaction  request failed
					handleLocationError(true);
				});
			// }catch(err){
			// 	console.log(err);
			// }
		} else {
			// Browser doesn't support Geolocation
			handleLocationError(false);
		}
	} else {
		// test look location Goodge Street tube station
		// studio.sizes.shin -> 
		var promise3 = new Promise(function(resolve, reject) {
			friendfinder.threewordstolatlng('studio.sizes.shin',function(latlng){
			    resolve(latlng);
			});
		});

		promise3
		.then(function(p2){
			console.log(p2);
			friendfinder.phase1(p2);
		});
	}
};
window.initphase1 = initphase1;

function initphase2(){
	//words=([a-z]+\.[a-z]+\.[a-z]+)
	let words = false;
	let wordsPattern = /words=(?:\/\/\/)*([a-z]+\.[a-z]+\.[a-z]+)/;
	let result = wordsPattern.exec(window.location.href);
	if (result) {
		words = result[1];
	}

	let friendfinder = new FriendFinder({
		"what3wordsAPIkey" : "2GNBA9OG",
		"googleAPIKey" : "AIzaSyDuFF1X2RZ7rzFejeTUp-ZlFjenju_DkJA",
		"mapcontainer" : ".googlemap"
	});
	friendfinder.phase2a(words);

};
window.initphase2 = initphase2;

function handleLocationError(browserHasGeolocation) {
	message = browserHasGeolocation ?
	'Error: The Geolocation service failed.' :
	'Error: Your browser doesn\'t support geolocation.';
	console.log(message);
	$('#message').text(message);
}

$(document).ready(function () {
	/* ---------------------------------------------------------------------------- */

	// useful stuff goes here


	/* ---------------------------------------------------------------------------- */
});
;