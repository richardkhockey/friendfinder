function initmap(){

	let message = 'geolocation is go.';
	let pos = false;

	if (navigator.geolocation) {
		// try{
			navigator.geolocation.getCurrentPosition(function(position) {
				pos = {
					"lat" : position.coords.latitude,
					"lng" : position.coords.longitude
				};

				$('#lat').text(pos.lat);
				$('#lng').text(pos.lng);

				frienfinder = new FriendFinder({
					"what3wordsAPIkey" : "2GNBA9OG",
					"googleAPIKey" : "AIzaSyDuFF1X2RZ7rzFejeTUp-ZlFjenju_DkJA",
					"mapcontainer" : ".googlemap",
					"location" : pos,
					"mapcentre" : {
						"lat" : 51.513264368977296,
						"lng" : -0.11471673956293671
					},
				});

			}, function() {
				// geoloaction  request failed
				handleLocationError(true);
			});
		// }catch(err){
		// 	console.log(err);
		// }
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false);
	}

}
window.initmap = initmap;

	"init" : function () {
		let that = this;

		if ( consoleLog ) {console.log("FriendFinder.init"); }

		if ( this.location ) {
			this.watchLocation = true;
		}

		this.googleMap = new GoogleMap({
			"APIKey" : "AIzaSyDuFF1X2RZ7rzFejeTUp-ZlFjenju_DkJA",
			"mapcontainer" : this.mapcontainer,
			"defaultSettings" : {
				/* "styles" : [
				  {
				    "featureType": "road",
				    "stylers": [
				      {
				        "color": "#ff0000"
				      }
				    ]
				  }
				], */
				"zoom" : 11,
				"center" : new google.maps.LatLng( this.mapcentre.lat, this.mapcentre.lng ),
				"panControl" : true,
				"zoomControl" : true,
				"mapTypeControl" : false,
				"streetViewControl" : false,
				"clickableIcons" : false,
				"mapTypeControlOptions": {
					"mapTypeIds": [ 'roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map' ]
				}
			}
		});
		this.googleMap.defineCustomMarkers(PinTypes);
		this.directionsService = new google.maps.DirectionsService;

		let home = null;
		// set two points
		if ( this.location ) {
			// use device location
			home = { "lat" : this.location.lat, "lng" : this.location.lng };			
		} else {
			home = { "lat" : 51.59772129317902, "lng" : 0.018531116087388 };
		}
		
		let target = 'cloth.ends.cure'; // stack
		/*
		target = "stable.sheet.types"; // west of 68 buckingham road approx 270 degree bearing
		target = "rabble.choice.spoke"; // n/w of 68 buckingham road approx 270 degree bearing
		target = "deputy.feared.badge"; // n of 68 Buckingham road

		target = "ledge.steps.foods"; // ss/w
		target = "ledge.steps.food"; // New England, USA

		target = "loose.earth.formed"; // ss/e south woodford tube station
		target = "forms.plates.rubble"; // Warren Street tube station
		target = "studio.sizes.shin"; // Goodge Street tube station
		*/
		target = "juices.codes.labs"; // mountain warehouse

		this.p1 = {
			"words" : null,
			"lat" : home.lat,
			"lng" : home.lng
		};
		this.p2 = {
			"words" : target,
			"lat" : null,
			"lng" : null
		};

		var promise3 = new Promise(function(resolve, reject) {
			that.latlngtothreewords(home,function(words){
			    resolve(words);
			});
		});

		promise3
		.then(function(value){

			that.p1.words = value;

			return new Promise((resolve, reject) => {
				that.threewordstolatlng(target,function(value){
				    resolve(value);
				});
			});
		}).then(function(value){
			that.p2.lat = value.lat;
			that.p2.lng = value.lng;

			$('span#lat1').text(that.p1.lat);
			$('span#lng1').text(that.p1.lng);
			$('span#threewords1').text(that.p1.words);

			$('span#lat2').text(that.p2.lat);
			$('span#lng2').text(that.p2.lng);
			$('span#threewords2').text(that.p2.words);

			let db = that.distanceAndBearing(that.p1,that.p2);
			// $('span#d').text( Math.round(db.distance / 1000) + 'km' );
			$('span#d').text( (db.distance / 1000).toFixed(2) + ' km' );
			$('span#b').text( db.bearing.toFixed(2) );
			let gauge = (db.distance > 1000) ? (db.distance / 1000).toFixed(2) + ' km' : db.distance.toFixed(0)  + ' m';
			$('span#dr').text( gauge );

			// display pointer showing bearing from -> to
			// find shortest angle to rotate pointer
			let degree = db.rotate.toFixed(2);
			let compassRose = $('.binnacle .compass img');
			compassRose.css({
                '-webkit-transform': 'rotate(' + degree + 'deg)',
                '-moz-transform': 'rotate(' + degree + 'deg)',
                '-ms-transform': 'rotate(' + degree + 'deg)',
                '-o-transform': 'rotate(' + degree + 'deg)',
                'transform': 'rotate(' + degree + 'deg)',
                'zoom': 1
            });
			
			that.plotPoints(that.p1,that.p2);

			/*
			// rotate entire compass so that pointer points to top
			let spinDelay = window.setTimeout(function(){

				let degree = -db.bearing.toFixed(2);
				// find least bearing difference
				if (degree < -180) { degree = 360 + degree; }

				let compassRose = $('.binnacle .compass');
				compassRose.css({
	                '-webkit-transform': 'rotate(' + degree + 'deg)',
	                '-moz-transform': 'rotate(' + degree + 'deg)',
	                '-ms-transform': 'rotate(' + degree + 'deg)',
	                '-o-transform': 'rotate(' + degree + 'deg)',
	                'transform': 'rotate(' + degree + 'deg)',
	                'zoom': 1
	            });

			},2000);
			*/

			// watch device location - update distance and bearing to p2
			// update every second
			/*
			if (that.watchLocation) {
				window.setInterval(function(){
					that.updateLocation();

				}, 1000);
			}
			*/

			// Opera throws warnings if you use geolocation without user interaction
			// trigger location update on button click instead
			$('#updatePosition').on('click',function(event){
				event.preventDefault();
				that.updateLocation();
			});

		}).finally(function(){
			console.log('finally');
		})

	},
