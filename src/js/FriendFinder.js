import GoogleMap from './GoogleMap.js';
import PinTypes from './PinTypes'

/* ------------------------------------------------ */
// FriendFinder
/* ------------------------------------------------ */

function FriendFinder(parameters) {
	this.parameters = parameters;

	this.what3wordsAPIkey = parameters.what3wordsAPIkey;
	this.googleAPIKey = parameters.googleAPIKey;
	this.mapcontainer = parameters.mapcontainer;

	this.watchLocation = true; // enable/disable continuous position updates on device
	this.heading = 0;
	this.northOffset = 0;

	this.init();
}

FriendFinder.prototype = {
	"constructor" : FriendFinder,

	"template" : function () { var that = this; },

	"init" : function () {
		let that = this;

		if ( consoleLog ) {console.log("FriendFinder.init"); }


	},
	"phase1" : function (position) {
		let that = this;
		// position = {"lat" : <float>,"lng" : <float>}
		if ( consoleLog ) {console.log("FriendFinder.phase1 lat: %s Lng: %s", position.lat, position.lng); }
		this.p2 = position;

		// draw map centred on position
		this.drawMap(new google.maps.LatLng( position.lat, position.lng ));

		// plot pin on map
		let points = [];
		points.push({
			"name" : "pingreen",
			"lat" : position.lat,
			"lng" : position.lng,
			"caption" : "You are here",
			"visible" : true
		});
		this.googleMap.placeMarkers(points);

		// waht3words API: lat,lng to words
		var promise3 = new Promise(function(resolve, reject) {
			that.latlngtothreewords({"lat" : position.lat, "lng" : position.lng},function(words){
			    resolve(words);
			});
		});

		promise3
		.then(function(value){
			// display what3words and build link to phase2
			$('#linktophase2').attr({"href" : "phase2.html?words=" + value}).text(value);
			$('.wordbutton').addClass('ready');
		});
	},
	"phase2a" : function (words) {
		let that = this;
		let manualEntry = (!words);
		words = (manualEntry) ? 'not specified/invalid entry - manual entry' : words;
		if ( consoleLog ) {console.log("FriendFinder.phase2 words: %s", words); }

		if (!manualEntry) {
			$('#p1words').val(words);
		}
		// remote destination form 
		$('#goToPhase2b').on('click',function(event){
			event.preventDefault();

			// validate #p1words
			let words = $('#p1words').val()
			// ///word.word.word
			// word.word.word
			let wordsPattern = /(?:\/\/\/)*([a-z]+\.[a-z]+\.[a-z]+)/;
			let result = wordsPattern.exec(words);
			if (result) {
				$('div.field').removeClass('hasErrors');
				$('div.field p.errorMessage').text('');

				// make sure words are lower case
				that.p2words = result[1];
				that.phase2b();
			} else {
				$('div.field p.errorMessage').text('invalid location code');
				$('div.field').addClass('hasErrors');
				console.log('invalid location code');
			}

		});
	},
	"phase2b" : function () {
		let that = this;
		if ( consoleLog ) {console.log("FriendFinder.phase2"); }

		this.compassRose = document.querySelector(".binnacle .compass");

		// get device location (p1)
		// optional: p1 LatLng -> what3words
		// what3words API words -> LatLng (p2)
		var promise3 = new Promise(function(resolve, reject) {
			that.threewordstolatlng(that.p2words,function(latlng){
			    resolve(latlng);
			});
		});

		promise3
		.then(function(p2){
			that.p2 = { "lat" : p2.lat, "lng" : p2.lng };

			let pos = false;
			if (navigator.geolocation) {
				// try{
					navigator.geolocation.getCurrentPosition(function(position) {
						pos = {
							"lat" : position.coords.latitude,
							"lng" : position.coords.longitude
						};
						that.p1 = pos;

						// include in update loop START
						that.updateCompassPointer();
						// include in update loop END

						// draw map centred on position
						that.drawMap(new google.maps.LatLng( p2.lat, p2.lng ));

						that.plotPoints({
							"lat" : pos.lat,
							"lng" : pos.lng,
							"caption" : "You"
						},
						{
							"lat" : p2.lat,
							"lng" : p2.lng,
							"caption" : "Your friend - " + that.p2words
						});
						$('#threewords2').text(that.p2words);
						$('#lat2').text(that.p2.lat);
						$('#lng2').text(that.p2.lng);

						$('.phase2a').removeClass('open');
						$('.phase2b').addClass('open');

						// update loop
						if (that.watchLocation) {
							window.setInterval(function(){
								that.updateLocation();
							}, 1000);
						}

						// device fine orientation
					    var compass = new MotionStack.Compass();
					    compass.start( function(event){
					    	that.deviceFineTracking(event);
					    });

					    // device coarse orientation
				        let woDelay = window.setTimeout(function(){
							that.deviceCoarseTracking()
					    }, 500);

						window.onorientationchange = function(event){
							that.deviceCoarseTracking()
						};  

						// rotate compass rose
					    window.requestAnimationFrame(function loop() {
					      if(that.heading !== undefined) {
					        that.rotateCompass();
					      }

					      window.requestAnimationFrame(loop);
					    });
					}, function() {
						// geoloaction  request failed
						that.handleLocationError(true);
					});
				// }catch(err){
				// 	console.log(err);
				// }
			} else {
				// Browser doesn't support Geolocation
				that.handleLocationError(false);
			}
		});

		// loop:
		//   update this device position
		//   calculate bearing and distance from p1 to p2
		//   update compass & distance display
	},
	"deviceFineTracking" : function (event){
		let that = this;
    	// block desktop mouse events when using the compass
    	if ( !( event.source =='touch' || event.source === 'pointermove' ) ) {
			that.heading = event.heading;
			// heading minus orientation offset
			let displayedHeading = that.heading - that.northOffset;

			// map result into range 0 - 360
			displayedHeading = (displayedHeading > 360) ? (displayedHeading - 360) : displayedHeading;
			displayedHeading = (displayedHeading < 0) ? (displayedHeading + 360) : displayedHeading;

			// reverse heading range
			displayedHeading = (360 - displayedHeading);

			$('#liveheading').text(displayedHeading.toFixed(2));
    	}
	},
	"deviceCoarseTracking" : function (event){
		let that = this;

		let orientation = null;
		try{
			let orientation = window.orientation;
		    if ( typeof orientation !== 'undefined') {

			    /*
			    switch(orientation) {
			    	case -90 : { $('#do').text('landscape right side down'); } break;
			    	case 90 : { $('#do').text('landscape left side down'); } break;
			    	default : { $('#do').text('portrait'); };
			    }
			    */

			    that.northOffset = orientation;
		    }
		}catch(err) {
		}
	},
	"rotateCompass" : function (){
		let that = this;

		this.compassRose.style.webkitTransform = "rotateZ("+ (this.heading - this.northOffset) + "deg) translateZ(0)";
		this.compassRose.style.transform = this.compassRose.style.webkitTransform;
	},
	"latlngtothreewords" : function (latlng,callback) {
		let that = this;

		$.ajax({
			"type" : "GET",
			"url" : "https://api.what3words.com/v3/convert-to-3wa?coordinates=" + latlng.lat + "," + latlng.lng + "&key=" + this.what3wordsAPIkey,
			"data" : {
			},
			"dataType" : "text",
			"cache" : false,
			"success" : function (text) {
				let response = $.parseJSON(text);

				callback(response.words);
			},
			"complete" : function () {
				console.log("complete");
			}
		});
	},
	"threewordstolatlng" : function (threewords,callback) {
		let that = this;

		$.ajax({
			"type" : "GET",
			"url" : "https://api.what3words.com/v3/convert-to-coordinates?words=" + threewords + "&key=" + this.what3wordsAPIkey,
			"data" : {
			},
			"dataType" : "text",
			"cache" : false,
			"success" : function (text) {
				let response = $.parseJSON(text);

				callback(response.coordinates);
			},
			"complete" : function () {
				console.log("complete");
			}
		});
	},
	"updateCompassPointer" : function () {
		let that = this;
		// calculate bearing and distance from p1 to p2
		let db = that.distanceAndBearing(that.p1,that.p2);
		$('span#d').text( (db.distance / 1000).toFixed(2) + ' km' );
		$('span#b').text( db.bearing.toFixed(2) );
		let gauge = (db.distance > 1000) ? (db.distance / 1000).toFixed(2) + ' km' : db.distance.toFixed(0)  + ' m';
		$('span#dr').text( gauge );

		// update compass & distance display
		// display pointer showing bearing from -> to
		// find shortest angle to rotate pointer
		let degree = db.rotate.toFixed(2);
		let compassRose = $('.binnacle .compass img');
		compassRose.css({
	        '-webkit-transform': 'rotate(' + degree + 'deg)',
	        '-moz-transform': 'rotate(' + degree + 'deg)',
	        '-ms-transform': 'rotate(' + degree + 'deg)',
	        '-o-transform': 'rotate(' + degree + 'deg)',
	        'transform': 'rotate(' + degree + 'deg)',
	        'zoom': 1
	    });
	},
	"updateLocation" : function () {
		let that = this;

		navigator.geolocation.getCurrentPosition(function(position) {

			that.p1 = {
				"lat" : position.coords.latitude,
				"lng" : position.coords.longitude,
				"words" : '-'
			};

			let points = [];
			points.push({
				"name" : "pingreen",
				"lat" : position.lat,
				"lng" : position.lng,
				"caption" : "You are here",
				"visible" : true
			});
			that.googleMap.placeMarkers(points);

			that.updateCompassPointer();
			/*
			// update what3words
			var promise3 = new Promise(function(resolve, reject) {
				that.latlngtothreewords({"lat" : position.coords.latitude, "lng" : position.coords.longitude},function(words){
				    resolve(words);
				});
			});

			promise3
			.then(function(words){
				console.log(words);
			});
			*/
		});
	},
	"drawMap" : function (mapcentre) {
		let that = this;

		this.googleMap = new GoogleMap({
			"APIKey" : "AIzaSyDuFF1X2RZ7rzFejeTUp-ZlFjenju_DkJA",
			"mapcontainer" : this.mapcontainer,
			"defaultSettings" : {
				/* "styles" : [
				  {
				    "featureType": "road",
				    "stylers": [
				      {
				        "color": "#ff0000"
				      }
				    ]
				  }
				], */
				"zoom" : 11,
				"center" : mapcentre,
				"panControl" : true,
				"zoomControl" : true,
				"mapTypeControl" : false,
				"streetViewControl" : false,
				"clickableIcons" : false,
				"mapTypeControlOptions": {
					"mapTypeIds": [ 'roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map' ]
				}
			}
		});
		this.googleMap.defineCustomMarkers(PinTypes);

	},
	"plotPoints" : function (p1,p2) {
		let that = this;

		let points = [];

		points.push({
			"name" : "pingreen",
			"lat" : p1.lat,
			"lng" : p1.lng,
			"caption" : p1.caption,
			"visible" : true
		});

		points.push({
			"name" : "pinred",
			"lat" : p2.lat,
			"lng" : p2.lng,
			"caption" : p2.caption,
			"visible" : true
		});

		that.googleMap.placeMarkers(points);

		// fit map to p1,p2
		let bounds = new google.maps.LatLngBounds();
		bounds.extend(new google.maps.LatLng( p1.lat, p1.lng ));
		bounds.extend(new google.maps.LatLng( p2.lat, p2.lng ));
		that.googleMap._map.fitBounds(bounds);
	},
	"handleLocationError" : function(isLocationAvailable) {
		let that = this;
		console.log('handleLocationError');
	},
	"distanceAndBearing" : function (from, to) {
		let that = this;

		// https://developers.google.com/maps/documentation/javascript/reference/geometry#spherical
		// google.maps.geometry.spherical
		// computeDistanceBetween(from (LatLng), to (LatLng))
		// computeHeading(from (LatLng), to (LatLng))
		let fromO = new google.maps.LatLng( from.lat, from.lng );
		let toO = new google.maps.LatLng( to.lat, to.lng );

		let d = google.maps.geometry.spherical.computeDistanceBetween(fromO, toO);
		let b0 = google.maps.geometry.spherical.computeHeading(fromO, toO);
		let b = b0;

		// handle weird google geometry bearing 0 to 180 same , 180 - 360 => -180 - 0
		// useful for rotate transforms
		if (b < 0) {
			b = 360 + b;
		}

		return {
			"distance" : d,
			"bearing" : b,
			"rotate" : b0
		};
	}
}
// window.FriendFinder = FriendFinder;
export default FriendFinder;
