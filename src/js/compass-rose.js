	// MotionStack compass example script
    // motion controls
    let heading;
    function tracking(e) {
    	// block desktop mouse events when using the compass
    	if ( !( e.source =='touch' || e.source === 'pointermove' ) ) {
			heading = e.heading;
			// heading minus orientation offset
			let displayedHeading = heading - northOffset;

			// map result into range 0 - 360
			displayedHeading = (displayedHeading > 360) ? (displayedHeading - 360) : displayedHeading;
			displayedHeading = (displayedHeading < 0) ? (displayedHeading + 360) : displayedHeading;

			// reverse heading range
			displayedHeading = (360 - displayedHeading);

			$('#liveheading').text(displayedHeading.toFixed(2));
    	}
    }

	// need to handle portrait / landscape
	// compass is more or less accurate in portrait
	// 90 degrees off in landscape
    var compass = new MotionStack.Compass();
    compass.start(tracking);

    // animation loop
    var dial = document.querySelector(".binnacle .compass");

    function rotateDial() {
      dial.style.webkitTransform = "rotateZ("+ (heading - northOffset) +"deg) translateZ(0)";
      dial.style.transform = dial.style.webkitTransform;
    }

    var rotateThisDial = rotateDial.bind(this);
    window.requestAnimationFrame(function loop() {
      if(heading !== undefined) {
        rotateThisDial();
      }

      window.requestAnimationFrame(loop);
    });

    // detect device orientation

    // iOS Safai / Android Chrome
    // window.orientation
    // 0 - portrait (right way up)
    // 90 - landscape on left side
    // -90 - landscape on right side
	var orientation = screen.msOrientation || screen.mozOrientation || (screen.orientation || {}).type;
	console.log(orientation);

	function devRotate(){
		let orientation = null;
		try{
			let orientation = window.orientation;
		    if ( typeof orientation !== 'undefined') {

			    switch(orientation) {
			    	case -90 : { $('#do').text('landscape right side down'); } break;
			    	case 90 : { $('#do').text('landscape left side down'); } break;
			    	default : { $('#do').text('portrait'); };
			    }

			    northOffset = orientation;
		    }
		}catch(err) {
		}

	}

    let woDelay = window.setTimeout(function(){
		devRotate()
    }, 500);

	window.onorientationchange = function(event){
		devRotate()
	};  
