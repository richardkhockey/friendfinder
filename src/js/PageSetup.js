let PageSetup = function(){
	window.mobileWidth = false;
	window.windowSize = 0;
	window.isIOS = null;
	window.thisParallaxScroll = null;
	window.moduleMinHeight = 320;
	window.supportsCSSTransitions = false;
	window.supportsCSSAnimation = false;
	window.supportsSVG = false;
	window.supportsWebWorker = false;
	window.ishighdpi = false;
	window.isIE = false;
	window.dpi = 0;
	window.landscapeAspect = false;
	window.shortLandscape = false;
	window.doVisualUpdates = false;
	let isIE10 = false;
	let isIE11 = !!navigator.userAgent.match(/Trident\/7\./);

	window.requestAnimFrame = (function(callback) {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		};
	})();

	if (!window.console) {
		window.console = {
			"log" : function(){}
		}
	}

	Modernizr.addTest('isios', function() {
	    return (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) ? true : false;
	});

	/*
	https://www.paulirish.com/2009/throttled-smartresize-jquery-event-handler/
	Debounced Resize() jQuery Plugin
	August 11th 2009
	*/
	(function($,sr){

	  // debouncing function from John Hann
	  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	  let debounce = function (func, threshold, execAsap) {
	      let timeout;

	      return function debounced () {
	          var obj = this, args = arguments;
	          function delayed () {
	              if (!execAsap)
	                  func.apply(obj, args);
	              timeout = null;
	          };

	          if (timeout)
	              clearTimeout(timeout);
	          else if (execAsap)
	              func.apply(obj, args);

	          timeout = setTimeout(delayed, threshold || 100);
	      };
	  }
	  // smartresize 
	  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

	})(jQuery,'smartresize');

	// usage:
	$(window).smartresize(function(){
	  // code that takes it easy...
	});


	$(document).ready(function () {

		supportsCSSAnimation = $('html').hasClass('cssanimations');
		supportsCSSTransitions = $('html').hasClass('csstransitions');
		supportsSVG = $('html').hasClass('csstransitions');
		isIE = $('html').hasClass('ie') || isIE11 || isIE10;

		isIOS = Modernizr.isios;
		// id device supports devicePixelRatio check for retina display, otherwise assume standard res (IE 9/10) 
		if (typeof window.devicePixelRatio != 'undefined' ) {
			ishighdpi = ( window.devicePixelRatio > 1);	
			dpi = window.devicePixelRatio;
		} else {
			ishighdpi = false;
			dpi = 1;
		}

		if ( location.href.indexOf('iostest=1') !== -1 ) { isIOS = true; }

		// test for iOS devices - relevant to background image attachment/device pixel ratio
		if ( isIOS ) { $('html').addClass('ios'); } else { $('html').addClass('not-ios'); } 

		$('html').addClass('dpr' + dpi);

		windowSize = {
			"w" : $(window).width(),
			"h" : $(window).height(),
			"a" : $(window).width() / $(window).height()
		};

		if (windowSize.a > 1) {
			$('html').removeClass('portraitAspectRatio');
			$('html').addClass('landscapeAspectRatio');
			landscapeAspect = true;

			if (landscapeAspect && windowSize.h < 500) {
				shortLandscape = true;
				$('html').addClass('shortLandscape');
			} else {
				shortLandscape = false;
				$('html').removeClass('shortLandscape');
			}
		} else {
			$('html').removeClass('landscapeAspectRatio');
			$('html').addClass('portraitAspectRatio');
			landscapeAspect = false;
		}

		// mobileWidth = (windowSize.w < 768) || (windowSize.a < 1);
		mobileWidth = (windowSize.w < 1024) && (windowSize.a < 1);

		// console.log("isIOS : %s", (isIOS) ? "ios" : "not ios");
		// console.log("platform (based on window.width: %s): %s", windowSize.w, (mobileWidth) ? "mobile" : "desktop");
		// if ( landscapeAspect ) { console.log('landscape'); } else { console.log('portrait'); }
		// if ( shortLandscape ) { console.log('short landscape'); }

		document.addEventListener('visibilitychange', function(){
			doVisualUpdates = !document.hidden;
			/*
			if (doVisualUpdates) {
				console.log('visible');
			} else {
				console.log('hidden');
			}
			*/
		});

		/*
		if (window.Worker) {
			console.log("web worker supported");
			global.gridWorker = new Worker('dist/grid_worker.js');
		}
		*/

		/* ---------------------------------------------------------------------------- */

		// useful stuff goes here

		/* ---------------------------------------------------------------------------- */
	});

};

export default PageSetup;

