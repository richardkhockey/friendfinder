	/* ------------------------------------------------ */
	// GoogleMap
	/* ------------------------------------------------ */

	function GoogleMap(parameters) {
		this.parameters = parameters;

		this.mapcontainer = parameters.mapcontainer;
		this.defaultSettings = parameters.defaultSettings;

		this._map;
		this.customMarkerTypes = [];

		this.init();
	}
	
	GoogleMap.prototype = {
		"constructor" : GoogleMap,

		"template" : function () { var that = this; },

		"init" : function () {
			if ( consoleLog ) {console.log("GoogleMap.init"); }

			this._map = new google.maps.Map(document.querySelector(this.mapcontainer), this.defaultSettings);
		},

		"goto" : function (place) {
			// console.log("GoogleMap.goto");

			/*
			"place" : {
				"lat" : DOUBLE,
				"lng" : DOUBLE,
				"zoom" : INT
			}
			*/
			this._map.setCenter( new google.maps.LatLng( place.lat,place.lng ) );

			if (typeof place.zoom !== 'undefined') {
				this._map.setZoom(place.zoom);	
			}
			
		},

		// store customMarkerTypes in GoogleMap
		"defineCustomMarkers" : function(customMarkerArray) {
			// console.log("GoogleMap.defineCustomMarkers");

			customMarkerArray.map(function(customMarker){
				this.customMarkerTypes.push( this.defineCustomMarker(customMarker) );
			},this);
		},

		"defineCustomMarker" : function(marker) {
			// console.log("GoogleMap.defineCustomMarker");

			/*
			"marker" : {
				"name" : STRING
				"size" : { "width" : INT, "height" : INT },
				"position" : { "x": INT, "y" : INT },
				"focus" : { "x" : INT, "y" : INT },
				"path" : STRING
			}
			*/

			let newCustomMarker = new google.maps.MarkerImage(
				marker.path,
				new google.maps.Size(marker.size.width, marker.size.height),
				new google.maps.Point(marker.position.x,marker.position.y),
				new google.maps.Point(marker.focus.x,marker.focus.y)
			);

			return ({"name" : marker.name, "marker" : newCustomMarker});
		},

		"getCustomMarker" : function (name) {
			// console.log("GoogleMap.getCustomMarker");

			let requiredMarker = false;

			this.customMarkerTypes.map(function(customMarkerType){
				if ( customMarkerType.name === name) {
					requiredMarker = customMarkerType.marker;
				}
			},this);

			return requiredMarker;
		},

		// return array of marker objects to callee
		"placeMarkers" : function (markerArray) {
			// console.log("GoogleMap.placeMarkers");

			if ( Array.isArray(markerArray) ) {
				let newMarkerArray = [];
				markerArray.map(function(marker){
					newMarkerArray.push( this.placeMarker(marker) );
				},this);

				return newMarkerArray;
			} else {
				return false;
			}
		},

		"placeMarker" : function (marker) {
			// console.log("GoogleMap.placeMarker");

			// {"name" : STRING, "lat" : DOUBLE, "lng" : DOUBLE, "caption" : STRING, "visible" : BOOLEAN}
			let pin = false;
			if ( marker.name === 'default' ) {
				pin = this.makeDefaultMarker(marker);
			} else {
				let pinType = this.getCustomMarker(marker.name);
				if ( pinType ) {
					pin = this.makeCustomMarker(marker,pinType);
				} else {
					pin = this.makeDefaultMarker(marker);
				}
			}
			pin.setVisible(marker.visible);												
			return pin;
		},

		"makeDefaultMarker" : function(marker) {
			// console.log("GoogleMap.makeDefaultMarker");

			let pin = new google.maps.Marker({
				"position" : new google.maps.LatLng( marker.lat, marker.lng ),
				"map" : this._map,
				"title" : marker.caption
			});
			return pin;			
		},

		"makeCustomMarker" : function(marker, customMarkerType) {
			// console.log("GoogleMap.makeCustomMarker");

			let pin = new google.maps.Marker({
				"icon" : customMarkerType,
				"position" : new google.maps.LatLng( marker.lat, marker.lng ),
				"map" : this._map,
				"title" : marker.caption
			});
			return pin;											
		},

		// return array of polyline objects to callee
		"definePolyLines" : function (polylineArray) {
			// console.log("GoogleMap.definePolyLines");

			if ( Array.isArray(polylineArray) ) {
				let newPolylineArray = [];

				polylineArray.map(function(polyline){
					newPolylineArray.push( this.definePolyLine(polyline) );
				},this);

				return newPolylineArray;
				
			} else {
				return false;
			}

		},

		"definePolyLine" : function (polyline) {
			// console.log("GoogleMap.definePolyLine");

			/*
			"polyline" : {
				"path" : ARRAY of Lat,lng,
				"colour" : STRING
			}
			*/

		    let path = new google.maps.Polyline(polyline);
		    path.setMap(this._map);
		    path.setVisible(true);

			return path;
		},

		"definePolygon" : function (polygon) {
			let thisPolygon = new google.maps.Polygon(polygon);
			thisPolygon.setMap(this._map);
			thisPolygon.setVisible(true);

			return thisPolygon;
		},

		"drawKML" : function(kml) {

		},

		"drawGeoJSON" : function(geoJSON) {
		},

		"addMapEvent" : function(eventtype,callback) {
			this._map.addListener(eventtype, function(data) {
				callback(data);
			});
		},

		"setmapStyles" : function(styles) {
			this._map.setOptions({ "styles" : styles });
		}


	    /* ---------------------------------------------------------------------------------------------------------------- */

	}
	// window.GoogleMap = GoogleMap;
	export default GoogleMap;
